(ns nubank.controller.account
  (:require nubank.database.db
            :as
            db
            [nubank.database.db :as db]))

(defn add-account
  [account]
  (db/add-account account))