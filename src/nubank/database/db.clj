(ns nubank.database.db
  (:require [korma.db :as db]
            [korma.core :as core]))

(def db-connection
  (db/h2
    {:db "./resources/db/dbexplore.db"}))

(db/defdb korma-db db-connection)
(core/defentity accounts)

(defn add-account [account]
  (core/insert accounts
    (core/values account)))

(defn get-account [name]
  (core/select acconunts
    (core/where {:name name}))
  )