(ns nubank.schema.schema
  (:require
    [schema.core :as s]))

(s/defschema Pizza
   {(s/optional-key :id) Long
    :name s/Str
    (s/optional-key :description) s/Str
    :size (s/enum :L :M :S)
    :origin {:country (s/enum :FI :PO :BR)
             :city s/Str}})

(s/defschema User
  {:name s/Str
   :last-name s/Str
   :age s/Int})

(s/defschema Account
  {:user User
   :account-type (s/enum :CC :CP)
   :balance Double})