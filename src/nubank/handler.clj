(ns nubank.handler
  (:require [compojure.api.sweet :refer :all]
            [ring.util.http-response :refer :all]
            [nubank.schema.schema :as schema]
            [nubank.controller.plus :as controller.plus]
            [nubank.controller.echo :as controller.echo]
            [nubank.controller.account :as controller.account]))

(def app
  (api
    {:swagger
     {:ui "/"
      :spec "/swagger.json"
      :data {:info {:title "Nubank"
                    :description "Compojure Api nubank"}
             :tags [{:name "api", :description "some apis"}]}}}

    (context "/api" []
      :tags ["api"]

      (GET "/plus" []
        :return {:result Long}
        :query-params [x :- Long, y :- Long]
        :summary "adds two numbers together"
        (ok {:result (controller.plus/sum x y)}))

      (POST "/echo" []
        :return schema/Pizza
        :body [pizza schema/Pizza]
        :summary "echoes a Pizza"
        (ok (controller.echo/echo-pizza pizza)))
      (POST "/account" []
        :return schema/Account
        :body [account schema/Account]
        :summary "endpoint Account"
        (ok (controller.account/add-account account)))
      ;(GET "/account" [id]
      ;  :return schema/Account
      ;  :param id
      ;  (ok (controller.echo/echo-pizza id))))
))
